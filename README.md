# wxUnit

## Téléchargement
[Télécharger wxUnit](https://gitlab.com/Johjo/wxunit/-/tags?sort=updated_desc)


## Approval testing
La méthode verify permet d'utiliser l'approval testing.

### Paramétrage du reporter
Afin de pouvoir utiliser un outil de comparaison pour l'approval testing, il faut créer dans le répertoire de l'exécutable le fichier reporter.ini et l'initialiser ainsi

```ini
[Reporter]
command_line=C:\Program Files (x86)\WinMerge\WinMergeU.exe %1 %2 /e
```
